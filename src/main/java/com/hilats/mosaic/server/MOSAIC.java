package com.hilats.mosaic.server;

import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;

/**
 * @see https://www.w3.org/TR/annotation-vocab/
 */
public class MOSAIC {

    public static final String NAMESPACE = "http://highlatitud.es/mosaics/";

    public static final URI TYPE_MOSAIC;
    public static final URI TYPE_RESOURCE;
    public static final URI PRED_IN_MOSAIC;
    public static final URI PRED_THUMBNAIL;


    static {
        ValueFactory factory = ValueFactoryImpl.getInstance();

        TYPE_MOSAIC = factory.createURI(NAMESPACE, "Mosaic");
        TYPE_RESOURCE = factory.createURI(NAMESPACE, "Resource");
        PRED_IN_MOSAIC = factory.createURI(NAMESPACE, "inMosaic");
        PRED_THUMBNAIL = factory.createURI(NAMESPACE, "thumbnail");
    }
}
