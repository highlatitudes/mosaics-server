package com.hilats.mosaic.server;

import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;

/**
 * @see https://www.w3.org/TR/annotation-vocab/
 */
public class OA {

    public static final String NAMESPACE = "http://www.w3.org/ns/oa#";

    public static final URI FRAGMENT_SELECTOR;
    public static final URI HAS_SELECTOR;


    static {
        ValueFactory factory = ValueFactoryImpl.getInstance();

        FRAGMENT_SELECTOR = factory.createURI(NAMESPACE, "FragmentSelector");
        HAS_SELECTOR = factory.createURI(NAMESPACE, "hasSelector");
    }
}
