package com.hilats.mosaic.server;

import com.fasterxml.jackson.core.JsonParseException;
import com.github.jsonldjava.core.DocumentLoader;
import com.github.jsonldjava.core.JsonLdError;
import com.github.jsonldjava.core.RemoteDocument;
import com.github.jsonldjava.utils.JsonUtils;

import java.io.IOException;
import java.net.URL;

/**
 * @author pduchesne
 *         Created by pduchesne on 05/11/16.
 */
public class MosaicDocumentLoader
    extends DocumentLoader
{
    @Override
    public RemoteDocument loadDocument(String url) throws JsonLdError {
        URL obj = null;

        if (url.toString().equals("http://highlatitud.es/mosaics/mosaic-context.jsonld"))
            obj = this.getClass().getResource("/jsonld/mosaic-context.jsonld");
        else if (url.toString().equals("http://highlatitud.es/mosaics/mosaic-frame.jsonld"))
            obj = this.getClass().getResource("/jsonld/mosaic-frame.jsonld");
        else if (url.toString().equals("http://highlatitud.es/mosaics/resource-frame.jsonld"))
            obj = this.getClass().getResource("/jsonld/resource-frame.jsonld");
        else if (url.toString().equals("http://highlatitud.es/mosaics/mosaic-framing-context.jsonld"))
            obj = this.getClass().getResource("/jsonld/mosaic-framing-context.jsonld");

        if (obj != null)
            try {
                return new RemoteDocument(url, JsonUtils.fromURL(obj, getHttpClient()));
            } catch (final Exception e) {
                throw new JsonLdError(JsonLdError.Error.LOADING_REMOTE_CONTEXT_FAILED, url);
            }
        else
            return super.loadDocument(url);
    }
}
