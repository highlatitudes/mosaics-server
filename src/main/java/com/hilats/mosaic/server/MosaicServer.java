package com.hilats.mosaic.server;

import com.hilats.mosaic.server.rest.resources.MosaicResource;
import com.hilats.mosaic.server.types.Mosaic;
import com.hilats.server.RdfApplication;
import com.hilats.server.RepoConnection;
import com.hilats.server.TripleStore;
import com.hilats.server.spring.SpringServletJerseyContainer;
import com.hilats.server.spring.jwt.HilatsUser;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.internal.inject.Injections;
import org.glassfish.jersey.server.spi.Container;
import org.glassfish.jersey.server.spi.ContainerLifecycleListener;
import org.openrdf.rio.jsonld.JSONStatementsReaderWriter;
import com.hilats.server.sesame.Skolemizer;
import com.hilats.server.sesame.TypedModel;
import org.openrdf.model.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

import javax.inject.Inject;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.core.MediaType;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by pduchesne on 1/08/14.
 */

public class MosaicServer
    extends RdfApplication
{

    @Autowired
    ThumbnailServer thumbnailServer;

    public MosaicServer(MosaicTripleStore store, Object... components) {
        super(store, components);
        init();
    }

    protected MosaicServer(TripleStore store, Resource[] initData, String mimeType, Object... components) throws FileNotFoundException {
        super(store, initData, mimeType, components);
        init();
    }

    private void init() {

        this.packages(MosaicServer.class.getPackage().getName()+".rest.resources");
        //this.register(LocatorSetFeature.class);

    }

    /* Hack to get hold of the ServiceLocator
    static ServiceLocator serviceLocator;
    static final class LocatorSetFeature implements Feature {

        private final ServiceLocator scopedLocator;

        @Inject
        private LocatorSetFeature(ServiceLocator scopedLocator) {
            this.scopedLocator = scopedLocator;
        }

        @Override
        public boolean configure(FeatureContext context) {
            serviceLocator = scopedLocator;
            return true;
        }
    }
    */

    @Override
    public MosaicTripleStore getStore() {
        return (MosaicTripleStore)super.getStore();
    }

    @Override
    public void initData() throws Exception {
        if (cleanOnInit) {
            getStore().clean();
        }

        if (initData != null) {
            if ("application/json".equals(initMimeType)) {
                RepoConnection conn = getConnFactory().getCurrentConnection();
                try {
                    // the default Sesame rio readers do not support plain json
                    // need to use the registered readerWriter with framing
                    List<? extends JSONStatementsReaderWriter> jsonReaders = findRegisteredComponents(JSONStatementsReaderWriter.class);
                    if (jsonReaders.size() == 0)
                        throw new IllegalStateException("Init data in JSON format, but not registered reader for JSON");
                    for (Resource res: initData) {
                        Model statements = jsonReaders.get(0).readFrom(TypedModel.class, Mosaic.class, null, MediaType.APPLICATION_JSON_TYPE, null, res.getInputStream()).getModel();

                        getStore().putMosaics(statements, new HilatsUser("admin", "", Arrays.asList("admin")));
                    }
                } finally {
                    getConnFactory().closeCurrentConnection();
                }
            } else {
                super.initData();
            }
        }
    }

    public ThumbnailServer getThumbnailServer() {
        return thumbnailServer;
    }
}
