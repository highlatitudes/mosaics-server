package com.hilats.mosaic.server.sesame;

import com.hilats.mosaic.server.MOSAIC;
import com.hilats.mosaic.server.MosaicTripleStore;
import com.hilats.mosaic.server.OA;
import com.hilats.mosaic.server.ThumbnailServer;
import com.hilats.server.sesame.SesameConnectionFactory;
import com.hilats.server.sesame.SesameTripleStore;
import com.hilats.server.sesame.Skolemizer;
import com.hilats.server.spring.jwt.HilatsUser;
import info.aduna.iteration.Iterations;
import org.openrdf.OpenRDFException;
import org.openrdf.model.*;
import org.openrdf.model.impl.LinkedHashModel;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.model.util.GraphUtil;
import org.openrdf.model.util.Models;
import org.openrdf.model.vocabulary.DC;
import org.openrdf.model.vocabulary.DCTERMS;
import org.openrdf.model.vocabulary.GEO;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.query.*;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.memory.MemoryStore;
import org.springframework.util.Assert;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.*;

/**
 * Created by pduchesne on 1/08/14.
 */
public class SesameMosaicTripleStore
    extends SesameTripleStore
    implements MosaicTripleStore
{

    private ThumbnailServer thumbnailServer;

    public SesameMosaicTripleStore(SesameConnectionFactory connFactory) throws RepositoryException {
        super(connFactory);
    }

    public SesameMosaicTripleStore(SesameConnectionFactory connFactory, ThumbnailServer thumbnailServer) throws RepositoryException {
        super(connFactory);
        this.thumbnailServer = thumbnailServer;
    }

    @Override
    public Model getMosaic(String id) {
        try {
            Value iri = getSesameConnection().getValueFactory().createURI(id);
            GraphQuery q = getSesameConnection().prepareGraphQuery(QueryLanguage.SPARQL, MOSAICS_FULL);
            q.setBinding("mosaic", iri);

            GraphQueryResult res = q.evaluate();

            return QueryResults.asModel(res);
        } catch (Exception e) {
            throw new RuntimeException("Failed to query triple store for id "+id, e);
        }
    }

    @Override
    public boolean deleteMosaic(String id) {
        //TODO check permissions

        try {
            //TODO blind remove. should check whether all descendant objects must be removed
            Model moz = getMosaic(id);
            if (moz.size() == 0) return false;
            getSesameConnection().remove(moz);
            return true;
        } catch (Exception e) {
            throw new RuntimeException("Failed to query triple store for id "+id, e);
        }
    }

    @Override
    public Model getAnnotation(String id) {
        try {
            Value iri = getSesameConnection().getValueFactory().createURI(id);
            GraphQuery q = getSesameConnection().prepareGraphQuery(QueryLanguage.SPARQL, MosaicTripleStore.prepareAnnotationQuery(-1));
            q.setBinding("ann", iri);

            GraphQueryResult res = q.evaluate();

            return QueryResults.asModel(res);
        } catch (Exception e) {
            throw new RuntimeException("Failed to query triple store for id "+id, e);
        }
    }

    @Override
    public boolean deleteAnnotation(String id) {
        //TODO check permissions

        try {
            Model ann = getAnnotation(id);
            if (ann.size() == 0) return false;
            getSesameConnection().remove(ann);
            return true;
        } catch (Exception e) {
            throw new RuntimeException("Failed to query triple store for id "+id, e);
        }
    }

    @Override
    public Model getMosaics() {
        try {
            GraphQuery q = getSesameConnection().prepareGraphQuery(QueryLanguage.SPARQL, MOSAICS_FULL);
            GraphQueryResult res = q.evaluate();

            return QueryResults.asModel(res);
        } catch (Exception e) {
            throw new RuntimeException("Failed to query triple store for mosaics ", e);
        }
    }

    @Override
    public Model getUserMosaics(String userId) {
        try {
            RepositoryConnection conn = getSesameConnection();

            GraphQuery q = conn.prepareGraphQuery(QueryLanguage.SPARQL, MOSAIC_BY_USER);
            q.setBinding("user", conn.getValueFactory().createURI(MOSAIC.NAMESPACE, userId));
            GraphQueryResult res = q.evaluate();

            return QueryResults.asModel(res);
        } catch (Exception e) {
            throw new RuntimeException("Failed to query triple store for mosaics ", e);
        }
    }

    @Override
    public Model getMosaics(String queryStr, String userId) {
        try {
            RepositoryConnection conn = getSesameConnection();

            GraphQuery q = conn.prepareGraphQuery(QueryLanguage.SPARQL, MOSAICS_SUMMARY);
            if (userId != null)
                q.setBinding("creator", conn.getValueFactory().createURI(MOSAIC.NAMESPACE, userId));
            if (queryStr != null)
                q.setBinding("query", conn.getValueFactory().createLiteral(queryStr));
            GraphQueryResult res = q.evaluate();

            return QueryResults.asModel(res);
        } catch (Exception e) {
            throw new RuntimeException("Failed to query triple store for mosaics ", e);
        }
    }

    @Override
    public Model getResources(String queryStr, String searchUrl, boolean expanded) {
        try {
            RepositoryConnection conn = getSesameConnection();

            GraphQuery q = conn.prepareGraphQuery(QueryLanguage.SPARQL, RESOURCES);
            if (queryStr != null)
                q.setBinding("query", conn.getValueFactory().createLiteral(queryStr));
            if (searchUrl != null)
                q.setBinding("searchUrl", conn.getValueFactory().createURI(searchUrl));
            if (expanded)
                q.setBinding("expanded", conn.getValueFactory().createLiteral(true));
            GraphQueryResult res = q.evaluate();

            return QueryResults.asModel(res);
        } catch (Exception e) {
            throw new RuntimeException("Failed to query triple store for mosaics ", e);
        }
    }

    @Override
    public Model getAnnotations(String queryStr, String linkType, String bodyUrl, String targetUrl, String userId, int limit) {
        try {
            RepositoryConnection conn = getSesameConnection();

            GraphQuery q = conn.prepareGraphQuery(QueryLanguage.SPARQL, MosaicTripleStore.prepareAnnotationQuery(limit));
            if (queryStr != null && queryStr.trim().length()>0)
                q.setBinding("query", conn.getValueFactory().createLiteral(queryStr));
            if (bodyUrl != null && bodyUrl.trim().length()>0)
                q.setBinding("bodyUrl", conn.getValueFactory().createURI(bodyUrl));
            if (targetUrl != null && targetUrl.trim().length()>0)
                q.setBinding("targetUrl", conn.getValueFactory().createURI(targetUrl));
            if (linkType != null && linkType.trim().length()>0)
                q.setBinding("linkType", conn.getValueFactory().createURI(linkType));
            if (userId != null && userId.trim().length()>0)
                q.setBinding("user", conn.getValueFactory().createURI(MOSAIC.NAMESPACE, userId));

            GraphQueryResult res = q.evaluate();

            return QueryResults.asModel(res);
        } catch (Exception e) {
            throw new RuntimeException("Failed to query triple store for mosaics ", e);
        }
    }


    @Override
    public Object putAnnotations(Model statements, HilatsUser writer) {
        return this.putAnnotations(statements, writer, false, null);
    }

    @Override
    public Model putAnnotation(Model statements, String targetAnnotId, HilatsUser writer) {
        return (Model)this.putAnnotations(statements, writer, true, targetAnnotId);
    }

    private Object putAnnotations(Model statements, HilatsUser writer, boolean isUnique, String targetAnnotId) {
        boolean isAdmin = writer != null && writer.getRoles().contains("admin");

        try {

            // replace blank nodes
            Skolemizer skolemizer = new Skolemizer(MOSAIC.NAMESPACE);
            statements = skolemizer.process(statements);

            // Fill a temp repo with model
            Repository repo = new SailRepository(new MemoryStore());
            repo.initialize();
            RepositoryConnection con = repo.getConnection();
            con.add(statements);
            con.commit();

            // retrieve annotations IRIs
            List<BindingSet> ids = Iterations.asList(con.prepareTupleQuery(QueryLanguage.SPARQL, DISTINCT_ANNOT_IDS).evaluate());

            if (isUnique) {
                // a specific annotation is targeted --> enforce single annotation update
                if (ids.size() > 1) throw new IllegalArgumentException("Wrong number of input annotations, expecting 1, got "+ids.size());

                if (targetAnnotId != null) {
                    // this should be an update of a specific annotation
                    Value iri = ids.get(0).getValue("annot");
                    if (!iri.stringValue().equals(targetAnnotId))
                        throw new IllegalArgumentException("IRI mismatch : PUTting " + targetAnnotId + ", but got triples for " + iri.stringValue());
                }
            }

            List updatedIRIs = new ArrayList();

            for (BindingSet mozIds:ids) {
                Value iri = mozIds.getValue("annot");
                URI annotId = con.getValueFactory().createURI(iri.stringValue());
                Model m;
                if (skolemizer.getMapping().containsValue(iri)) {
                    // value has been generated in skolemizer --> it was not part of the posted content

                    // insert new
                    GraphQueryResult moz;
                    GraphQuery q = con.prepareGraphQuery(QueryLanguage.SPARQL, MosaicTripleStore.prepareAnnotationQuery(-1));
                    q.setBinding("ann", iri); // TODO re-use BindingSet ?

                    m = QueryResults.asModel(q.evaluate());

                    URI advertisedCreator = Models.anyObjectURI(m.filter(annotId, DC.CREATOR, null));

                    if (advertisedCreator != null) {
                        // the posted content already has a creator
                        // make sure current user has the rights to write for that creator
                        if (!advertisedCreator.getLocalName().equals(writer.getUsername()) && !isAdmin)
                            throw new SecurityException("Not allowed to create/update for other user "+advertisedCreator.getLocalName());
                    } else {
                        // set creator as current user
                        Models.setProperty(m, annotId, DC.CREATOR, con.getValueFactory().createURI(MOSAIC.NAMESPACE, writer.getUsername()));
                    }

                } else {
                    // there was an ID value in the posted content
                    // WARN doesn't mean it matches an existing annotation!

                    // try to update
                    GraphQuery q = con.prepareGraphQuery(QueryLanguage.SPARQL, MosaicTripleStore.prepareAnnotationQuery(-1));
                    q.setBinding("ann", iri); // TODO re-use BindingSet ?

                    m = QueryResults.asModel(q.evaluate());

                    Model existingAnnot = getAnnotation(iri.stringValue());
                    URI currentCreator = Models.anyObjectURI(existingAnnot.filter(annotId, DC.CREATOR, null));

                    URI advertisedCreator = Models.anyObjectURI(m.filter(annotId, DC.CREATOR, null));

                    if (currentCreator != null) {
                        // the mosaic has a creator in the database
                        // make sure current user has the rights to write for that creator
                        if (!currentCreator.getLocalName().equals(writer.getUsername()) && !isAdmin)
                            throw new SecurityException("Not allowed to update content from user "+currentCreator.getLocalName());
                    }

                    if (advertisedCreator != null) {
                        // the posted content already has a creator
                        // make sure current user has the rights to write for that creator
                        if (!advertisedCreator.getLocalName().equals(writer.getUsername()) && !isAdmin)
                            throw new SecurityException("Not allowed to create/update for other user "+advertisedCreator.getLocalName());
                    } else {
                        // set creator as current user
                        Models.setProperty(m, annotId, DC.CREATOR, con.getValueFactory().createURI(MOSAIC.NAMESPACE, writer.getUsername()));
                    }


                    // remove all statements for that annotation
                    //Assert.isTrue(
                    getSesameConnection().remove(existingAnnot);
                    //        "Annotation " + iri + " does not exist and cannot be updated");

                }

                // update dct:modified
                Models.setProperty(m, annotId, DCTERMS.MODIFIED, con.getValueFactory().createLiteral(new Date()));

                addModelStatements(m);
                updatedIRIs.add(iri.stringValue());
            }

            if (isUnique) {
                return getAnnotation((String)updatedIRIs.get(0));
            } else {
                //TODO return more than array of IRIs ? [{@id, @type}] ?
                return updatedIRIs;
            }


        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Object putMosaics(Model statements, HilatsUser writer) {
        return this.putMosaics(statements, null, writer);
    }

    @Override
    public Object putMosaics(Model statements, String mosaicId, HilatsUser writer) {
        boolean isAdmin = writer != null && writer.getRoles().contains("admin");

        try {

            // replace blank nodes
            Skolemizer skolemizer = new Skolemizer(MOSAIC.NAMESPACE);
            statements = skolemizer.process(statements);

            // Fill a temp repo with model
            Repository repo = new SailRepository(new MemoryStore());
            repo.initialize();
            RepositoryConnection con = repo.getConnection();
            con.add(statements);
            con.commit();

            // retrieve mosaic IRIs
            List<BindingSet> ids = Iterations.asList(con.prepareTupleQuery(QueryLanguage.SPARQL, DISTINCT_MOZ_IDS).evaluate());
            if (mosaicId != null) {
                // a specific mosaic is targeted --> enforce single mosaic update
                if (ids.size() != 1) throw new IllegalArgumentException("Wrong number of input mosaics, expecting 1, got "+ids.size());
                Value iri = ids.get(0).getValue("mosaic");
                if (!iri.stringValue().equals(mosaicId)) throw new IllegalArgumentException("IRI mismatch : PUTting "+mosaicId+", but got triples for "+iri.stringValue());
            }

            List updatedIRIs = new ArrayList();

            for (BindingSet mozIds:ids) {
                Value iri = mozIds.getValue("mosaic");
                URI mozId = con.getValueFactory().createURI(iri.stringValue());

                Model m;

                if (skolemizer.getMapping().containsValue(iri)) {
                    // value has been generated in skolemizer --> it was not part of the posted content

                    // insert new
                    GraphQuery q = con.prepareGraphQuery(QueryLanguage.SPARQL, MosaicTripleStore.MOSAIC_BY_URI_INFER_INMOSAIC);
                    q.setBinding("mosaic", iri); // TODO re-use BindingSet ?

                    m = QueryResults.asModel(q.evaluate());

                    URI advertisedCreator = Models.anyObjectURI(m.filter(mozId, DC.CREATOR, null));

                    if (advertisedCreator != null) {
                        // the posted content already has a creator
                        // make sure current user has the rights to write for that creator
                        if (!advertisedCreator.getLocalName().equals(writer.getUsername()) && !isAdmin)
                            throw new SecurityException("Not allowed to create/update for other user "+advertisedCreator.getLocalName());
                    } else {
                        // set creator as current user
                        Models.setProperty(m, mozId, DC.CREATOR, con.getValueFactory().createURI(MOSAIC.NAMESPACE, writer.getUsername()));
                    }


                } else {
                    // there was an ID value in the posted content
                    // WARN doesn't mean it matches an existing mosaic!

                    //TODO check user permission on this mosaic

                    // try to update
                    GraphQuery q = con.prepareGraphQuery(QueryLanguage.SPARQL, MosaicTripleStore.MOSAICS_FULL);
                    q.setBinding("mosaic", iri); // TODO re-use BindingSet ?

                    m = QueryResults.asModel(q.evaluate());

                    Model existingMoz = getMosaic(iri.stringValue());
                    URI currentCreator = Models.anyObjectURI(existingMoz.filter(mozId, DC.CREATOR, null));

                    URI advertisedCreator = Models.anyObjectURI(m.filter(mozId, DC.CREATOR, null));

                    if (currentCreator != null) {
                        // the mosaic has a creator in the database
                        // make sure current user has the rights to write for that creator
                        if (!currentCreator.getLocalName().equals(writer.getUsername()) && !isAdmin)
                            throw new SecurityException("Not allowed to update content from user "+currentCreator.getLocalName());
                    }

                    if (advertisedCreator != null) {
                        // the posted content already has a creator
                        // make sure current user has the rights to write for that creator
                        if (!advertisedCreator.getLocalName().equals(writer.getUsername()) && !isAdmin)
                            throw new SecurityException("Not allowed to create/update for other user "+advertisedCreator.getLocalName());
                    } else {
                        // set creator as current user
                        Models.setProperty(m, mozId, DC.CREATOR, con.getValueFactory().createURI(MOSAIC.NAMESPACE, writer.getUsername()));
                    }

                    // remove all statements for that mosaic
                    //Assert.isTrue(
                    getSesameConnection().remove(existingMoz);
                    //        "Mosaic " + iri + " does not exist and cannot be updated");

                }

                // update dct:modified
                Models.setProperty(m, mozId, DCTERMS.MODIFIED, con.getValueFactory().createLiteral(new Date()));

                addModelStatements(m);

                updatedIRIs.add(iri.stringValue());
            }

            //TODO return more than array of IRIs ? [{@id, @type}] ?
            return updatedIRIs;

        } catch (OpenRDFException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addModelStatements(Model statements) {

        Model selectors = statements.filter(null, OA.HAS_SELECTOR, null);
        for (Statement selectorSt: selectors) {
            URI fragmentUri = ValueFactoryImpl.getInstance().createURI(selectorSt.getObject().stringValue());
            String fragmentStr = statements.filter(fragmentUri, RDF.VALUE, null).objectString().toLowerCase();
            if (fragmentStr.contains("bbox")) {
                for (String fragment: fragmentStr.split("&")) {
                    String[] keyValue = fragment.split("=");
                    if ("bbox".equals(keyValue[0])) {
                        String[] x1y1x2y2 = keyValue[1].split(",");
                        /*
                        SpatialContext geo = SpatialContext.GEO;
                        Rectangle rect = geo.makeRectangle(
                                Double.valueOf(x1y1x2y2[0]),
                                Double.valueOf(x1y1x2y2[2]),
                                Double.valueOf(x1y1x2y2[1]),
                                Double.valueOf(x1y1x2y2[3]));
                                */
                        String bboxWkt = String.format("ENVELOPE (%s, %s, %s, %s)", x1y1x2y2[0], x1y1x2y2[2], x1y1x2y2[3], x1y1x2y2[1]);
                        statements.add(
                                selectorSt.getSubject(), // the body or target of the annotation
                                GEO.AS_WKT,
                                ValueFactoryImpl.getInstance().createLiteral(bboxWkt, GEO.WKT_LITERAL));
                    }
                }
            }
        }

        if (thumbnailServer != null) {
            Model thumbnails = statements.filter(null, MOSAIC.PRED_THUMBNAIL, null);
            Set<Statement> replaced = new HashSet();
            thumbnails.stream().forEach(statement -> {
                String url = statement.getObject().stringValue();
                if (url.startsWith("data:")) {
                    int dataPos = url.indexOf(',') + 1;
                    String header = url.substring(5, dataPos - 1);
                    String mimeType = header.substring(0, header.indexOf(";"));

                    // Enforce PNG as mime type
                    if (! "image/png".equals(mimeType))
                        throw new IllegalArgumentException("Thumbnails must be provided as PNG");

                    byte[] data = Base64.getDecoder().decode(url.substring(dataPos));

                    thumbnailServer.putThumbnail(((URI)statement.getSubject()).getLocalName()+".png", new ByteArrayInputStream(data));
                    replaced.add(statement);
                }

                // temp fix
                /*
                if ("local:thumbnail".equals(url)) {
                    File thumbnailFile = thumbnailServer.getThumbnailFile(((URI) statement.getSubject()).getLocalName());
                    thumbnailFile.renameTo(new File(thumbnailFile.getAbsolutePath()+".png"));
                    replaced.add(statement);
                }
                */
            });

            replaced.forEach(statement -> {
                thumbnails.remove(statement);
                thumbnails.add(statement.getSubject(), statement.getPredicate(), ValueFactoryImpl.getInstance().createLiteral("/thumbnails/" + ((URI) statement.getSubject()).getLocalName() + ".png"));
            });
        }

        super.addModelStatements(statements);
    }
}
