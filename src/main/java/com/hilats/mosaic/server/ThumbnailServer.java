package com.hilats.mosaic.server;

import java.io.*;
import java.nio.file.StandardCopyOption;

/**
 * @author pduchesne
 *         Created by pduchesne on 20/12/17.
 */
public class ThumbnailServer {

    private File rootDir;

    public ThumbnailServer(File rootDir) {
        this.rootDir = rootDir;
    }

    public File getRootDir() {
        return rootDir;
    }

    public void putThumbnail(String id, InputStream data) {
        try {
            java.nio.file.Files.copy(
                    data,
                    getThumbnailFile(id).toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException("Failed to write thumbnail", e);
        }
    }

    public InputStream getThumbnail(String id) {
        try {
            return new FileInputStream(getThumbnailFile(id));
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Failed to read thumbnail", e);
        }
    }

    public File getThumbnailFile(String id) {
        return new File(rootDir, id);
    }
}
