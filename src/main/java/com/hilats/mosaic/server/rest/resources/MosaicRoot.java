package com.hilats.mosaic.server.rest.resources;

import com.hilats.mosaic.server.rest.containers.wfs3.WFS;

import javax.ws.rs.Path;

/**
 * @author pduchesne
 *         Created by pduchesne on 10/02/18.
 */
@Path("/")
public class MosaicRoot {

    @Path("/wfs")
    public Class<WFS> getWFS() {
        return WFS.class;
    }
}
