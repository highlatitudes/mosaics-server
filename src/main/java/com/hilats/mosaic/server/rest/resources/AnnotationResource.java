package com.hilats.mosaic.server.rest.resources;

import com.github.jsonldjava.core.JsonLdError;
import com.hilats.mosaic.server.MosaicTripleStore;
import com.hilats.mosaic.server.types.Annotation;
import com.hilats.mosaic.server.types.Resource;
import com.hilats.server.rest.resources.Unique;
import com.hilats.server.sesame.Skolemizer;
import com.hilats.server.sesame.TypedModel;
import info.aduna.iteration.Iterations;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.GraphQueryResult;
import org.openrdf.query.QueryLanguage;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.jsonld.JSONStatementsReaderWriter;
import org.openrdf.sail.memory.MemoryStore;
import org.springframework.util.Assert;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.*;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("annotations")
public class AnnotationResource
    extends AbstractMosaicResource
{

    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({"application/ld+json", MediaType.APPLICATION_JSON, "text/turtle"})
    public TypedModel<Annotation> getJsonLd(Map filter) {

        if (filter == null)
            filter = new HashMap();

        String targetUrl = filter.containsKey("target") ? (String)((Map)filter.get("target")).get("source") : null;
        String bodyUrl = filter.containsKey("body") ? (String)((Map)filter.get("body")).get("source") : null;
        String linkType = filter.containsKey("body") ? (String)((Map)filter.get("body")).get("moz:linkType") : null;
        String owner = (String)(filter.get("owner"));
        if ("me".equals(owner)) {
            if (getUser() == null)
                throw new WebApplicationException("No valid user is authenticated", 401);
            owner = getUser().getUsername();
        }

        //TODO orderby
        Number limit = (Number)(filter.get("limit"));

        Model annotationsModel = (Model)getApplication().getStore().getAnnotations(
                (String)filter.get("text"),
                linkType,
                bodyUrl,
                targetUrl,
                owner,
                limit == null ? -1 : limit.intValue());

        return new TypedModel<Annotation>(annotationsModel);
    }

    @GET
    @Produces({"application/ld+json", MediaType.APPLICATION_JSON, "text/turtle"})
    public TypedModel<Annotation> getAnnotations() {

        Model annotationsModel = (Model)getApplication().getStore().getAnnotations(null, null, null, null, null, -1);

        return new TypedModel<Annotation>(annotationsModel);
    }

    @GET
    @Path("/mine")
    @Produces({"application/ld+json", MediaType.APPLICATION_JSON, "text/turtle"})
    public TypedModel<Annotation> getCurrentUserAnnotations() {

        if (getUser() == null)
            throw new WebApplicationException("No valid user is authenticated", 401);

        Model annotationsModel = (Model)getApplication().getStore().getAnnotations(null, null, null, null, getUser().getUsername(), -1);

        return new TypedModel<Annotation>(annotationsModel);
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON, "application/ld+json", "text/turtle"})
    @Produces({MediaType.APPLICATION_JSON})
    @RolesAllowed("user")
    public Object putJson(TypedModel<Annotation> typedStatements) {

        Model statements = typedStatements.getModel();

        return getApplication().getStore().putAnnotations(statements, getUser());
    }


    /**
     * Create an annotation
     * @param statements
     * @param id id of the mosaic to update
     */
    @PUT
    @Unique
    @Consumes({"application/ld+json", MediaType.APPLICATION_JSON})
    @RolesAllowed("user")
    public TypedModel<Annotation> createAnnotation(TypedModel<Annotation> typedStatements) throws JsonLdError {

        Model statements = typedStatements.getModel();
        com.github.jsonldjava.core.Context ctx = new com.github.jsonldjava.core.Context().parse(JSONStatementsReaderWriter.PARSE_CONTEXT.get("@context"));

        try {
            Model annotationModel = (Model)getApplication().getStore().putAnnotation(statements, null, getUser());
            return new TypedModel<Annotation>(annotationModel);
        } catch (SecurityException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.UNAUTHORIZED);
        }

    }

    /**
     * Update an annotation
     * @param statements
     * @param id id of the mosaic to update
     */
    @PUT
    @Path("/{id}")
    @Unique
    @Consumes({"application/ld+json", MediaType.APPLICATION_JSON})
    @RolesAllowed("user")
    public TypedModel<Annotation> updateAnnotation(TypedModel<Annotation> typedStatements, @PathParam("id") String id) throws JsonLdError {
        Model statements = typedStatements.getModel();
        com.github.jsonldjava.core.Context ctx = new com.github.jsonldjava.core.Context().parse(JSONStatementsReaderWriter.PARSE_CONTEXT.get("@context"));
        String expandedId = (String)((Map)ctx.expandValue("moz:inMosaic", id)).get("@id");

        try {
            Model annotationModel = (Model)getApplication().getStore().putAnnotation(statements, expandedId, getUser());
            return new TypedModel<Annotation>(annotationModel);
        } catch (SecurityException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.UNAUTHORIZED);
        }

    }

    @GET
    @Path("/{id}")
    @Unique
    @Produces({"application/ld+json", MediaType.APPLICATION_JSON, "text/turtle"})
    public TypedModel<Annotation> getAnnotationById(@PathParam("id") String id) throws JsonLdError {
        com.github.jsonldjava.core.Context ctx = new com.github.jsonldjava.core.Context().parse(JSONStatementsReaderWriter.PARSE_CONTEXT.get("@context"));
        String expandedId = (String)((Map)ctx.expandValue("moz:inMosaic", id)).get("@id");

        Model annotationModel = (Model)getApplication().getStore().getAnnotation(expandedId);
        if (annotationModel.size() == 0) throw new WebApplicationException(404);
        else return new TypedModel<Annotation>(annotationModel);
    }

    @GET
    @Path("/{id}")
    @Unique
    @Produces({MediaType.TEXT_HTML})
    public Response getAnnotationByIdHtml(@PathParam("id") String id, @Context HttpServletRequest request) throws JsonLdError {
        if (!id.startsWith("moz:") && !id.startsWith("http"))
            // assume this is an ID missing the namespace
            id = "moz:" + id;

        int idx = request.getRequestURL().indexOf("/api/annotations");
        String htmlUrl = request.getRequestURL().substring(0, idx) + "#/preview?ann="+id;

        return Response.temporaryRedirect(URI.create(htmlUrl)).build();
    }


    @DELETE
    @Path("/{id}")
    @RolesAllowed("user")
    public void deleteAnnotation(@PathParam("id") String id) throws JsonLdError {

        com.github.jsonldjava.core.Context ctx = new com.github.jsonldjava.core.Context().parse(JSONStatementsReaderWriter.PARSE_CONTEXT.get("@context"));
        String expandedId = (String)((Map)ctx.expandValue("moz:inMosaic", id)).get("@id");
        if (!getApplication().getStore().deleteAnnotation(expandedId))
            throw new WebApplicationException(404);
    }

}
