package com.hilats.mosaic.server.rest.resources;

import com.github.jsonldjava.core.JsonLdError;
import com.hilats.mosaic.server.MosaicTripleStore;
import com.hilats.server.rest.resources.Unique;
import org.openrdf.rio.jsonld.JSONStatementsReaderWriter;
import com.hilats.server.sesame.Skolemizer;
import com.hilats.server.sesame.TypedModel;
import info.aduna.iteration.Iterations;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.query.*;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.memory.MemoryStore;
import org.springframework.util.Assert;
import com.hilats.mosaic.server.types.Mosaic;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.*;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("mosaics")
public class MosaicResource
    extends AbstractMosaicResource
{

    /**
     * Create or update potentially multiple mosaics from a set of triples
     *
     * @param statements
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON, "application/ld+json", "text/turtle"})
    @Produces({MediaType.APPLICATION_JSON})
    @RolesAllowed("user")
    public Object putJson(TypedModel<Mosaic> typedStatements) {

        Model statements = typedStatements.getModel();

        try {
            return getApplication().getStore().putMosaics(statements, getUser());
        } catch (SecurityException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.UNAUTHORIZED);
        }
    }


    /**
     * Update a mosaic resource
     * @param statements
     * @param id id of the mosaic to update
     */
    @PUT
    @Path("/{id}")
    //TODO add Unique annotation and return updated object
    @Consumes({"application/ld+json", MediaType.APPLICATION_JSON})
    @RolesAllowed("user")
    public void putMosaic(TypedModel<Mosaic> typedStatements, @PathParam("id") String id) throws JsonLdError {
        Model statements = typedStatements.getModel();
        com.github.jsonldjava.core.Context ctx = new com.github.jsonldjava.core.Context().parse(JSONStatementsReaderWriter.PARSE_CONTEXT.get("@context"));
        String expandedId = (String)((Map)ctx.expandValue("moz:inMosaic", id)).get("@id");

        try {
            getApplication().getStore().putMosaics(statements, expandedId, getUser());
        } catch (SecurityException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.UNAUTHORIZED);
        }
    }

    @GET
    @Produces({"application/ld+json", MediaType.APPLICATION_JSON, "text/turtle"})
    public TypedModel<Mosaic> getJsonLd(@QueryParam("sparql") String sparql) {

        Model mosaicsModel = (Model)getApplication().getStore().getMosaics();

        return new TypedModel<Mosaic>(mosaicsModel);
    }

    @GET
    @Path("/mine")
    @Produces({"application/ld+json", MediaType.APPLICATION_JSON, "text/turtle"})
    public TypedModel<Mosaic> getCurrentUserMosaics() {

        if (getUser() == null)
            throw new WebApplicationException("No valid user is authenticated", 401);

        Model mosaicsModel = (Model)getApplication().getStore().getUserMosaics(getUser().getUsername());

        return new TypedModel<Mosaic>(mosaicsModel);
    }

    @POST
    @Path("/search")
    @Produces({"application/ld+json", MediaType.APPLICATION_JSON, "text/turtle"})
    public TypedModel<Mosaic> searchMosaics(Map filter) {

        if (filter == null)
            filter = new HashMap();

        Model mosaicsModel = (Model)getApplication().getStore().getMosaics((String)filter.get("text"), (String)filter.get("creator"));

        return new TypedModel<Mosaic>(mosaicsModel);
    }

    @GET
    @Path("/{id}")
    @Unique
    @Produces({"application/ld+json", MediaType.APPLICATION_JSON, "text/turtle"})
    public TypedModel<Mosaic> getMosaicById(@PathParam("id") String id) throws JsonLdError {
        if (!id.startsWith("moz:") && !id.startsWith("http"))
            // assume this is an ID missing the namespace
            id = "moz:" + id;

        com.github.jsonldjava.core.Context ctx = new com.github.jsonldjava.core.Context().parse(JSONStatementsReaderWriter.PARSE_CONTEXT.get("@context"));
        String expandedId = (String)((Map)ctx.expandValue("moz:inMosaic", id)).get("@id");

        Model mosaicModel = (Model)getApplication().getStore().getMosaic(expandedId);
        if (mosaicModel.size() == 0) throw new WebApplicationException(404);
        else return new TypedModel<Mosaic>(mosaicModel);
    }

    @GET
    @Path("/{id}")
    @Unique
    @Produces({MediaType.TEXT_HTML})
    public Response getMosaicByIdHtml(@PathParam("id") String id, @Context HttpServletRequest request) throws JsonLdError {
        if (!id.startsWith("moz:") && !id.startsWith("http"))
            // assume this is an ID missing the namespace
            id = "moz:" + id;

        int idx = request.getRequestURL().indexOf("/api/mosaics");
        String htmlUrl = request.getRequestURL().substring(0, idx) + "/#/mosaics/"+id;

        return Response.temporaryRedirect(URI.create(htmlUrl)).build();
    }

    @DELETE
    @Path("/{id}")
    @RolesAllowed("user")
    public void deleteMosaic(@PathParam("id") String id) throws JsonLdError {

        com.github.jsonldjava.core.Context ctx = new com.github.jsonldjava.core.Context().parse(JSONStatementsReaderWriter.PARSE_CONTEXT.get("@context"));
        String expandedId = (String)((Map)ctx.expandValue("moz:inMosaic", id)).get("@id");

        boolean found;

        try {
            found = getApplication().getStore().deleteMosaic(expandedId);
        } catch (SecurityException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.UNAUTHORIZED);
        }

        if (!found)
            throw new WebApplicationException(404);
    }
}
