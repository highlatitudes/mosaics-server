package com.hilats.mosaic.server.rest.containers.wfs3;

import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.servers.Server;

import javax.annotation.PostConstruct;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.util.Enumeration;

/**
 * @author pduchesne
 *         Created by pduchesne on 09/02/18.
 */
@OpenAPIDefinition(
        servers = {
                @Server(
                        description = "definition server 1",
                        url = "http://definition1")
        }
)
@Path("/")
public class WfsOpenApiResource
    extends OpenApiResource {

    @Context
    ServletConfig config;

    @Context
    Application app;

    @PostConstruct
    private void init() {
        this.setConfigLocation("mosaics-openapi-configuration.json");
    }

    @Override
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getOpenApi(@Context HttpHeaders headers, @Context UriInfo uriInfo, String type) throws Exception {
        final ServletConfig parentConfig = this.config;
        ServletConfig config = new ServletConfig() {
            @Override
            public String getServletName() {
                return "mosaics-openapi";
            }

            @Override
            public ServletContext getServletContext() {
                return parentConfig.getServletContext();
            }

            @Override
            public String getInitParameter(String name) {
                return null;
            }

            @Override
            public Enumeration<String> getInitParameterNames() {
                return null;
            }
        };

        return super.getOpenApi(headers, config, app, uriInfo, "json");
    }

}
