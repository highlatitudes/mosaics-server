package com.hilats.mosaic.server.rest.resources;

import com.hilats.mosaic.server.types.Resource;
import com.hilats.server.sesame.TypedModel;
import org.openrdf.model.Model;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Map;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("resources")
public class ResourceResource
    extends AbstractMosaicResource
{

    /**
     *
     * @param queryStr
     * @param expanded if true, expand search results to annotation targets that may not be explicit resources
     * @return
     */
    @GET
    @Produces({"application/ld+json", MediaType.APPLICATION_JSON, "text/turtle"})
    public TypedModel<Resource> getJsonLd(@QueryParam("query") String queryStr, @QueryParam("expanded") boolean expanded) {

        Model resourcesModel = (Model)getApplication().getStore().getResources(queryStr, null, expanded);

        return new TypedModel<Resource>(resourcesModel);
    }

    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({"application/ld+json", MediaType.APPLICATION_JSON, "text/turtle"})
    public TypedModel<Resource> getJsonLd(Map filter) {

        Model resourcesModel = (Model)getApplication().getStore().getResources(
                (String)filter.get("text"),
                (String)filter.get("url"),
                Boolean.valueOf((String)filter.get("expanded"))
                );

        return new TypedModel<Resource>(resourcesModel);
    }

}
