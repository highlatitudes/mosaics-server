package com.hilats.mosaic.server.rest.containers.wfs3;


import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.annotations.Operation;

import javax.annotation.PostConstruct;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author pduchesne
 *         Created by pduchesne on 10/02/18.
 */
@Path("/")
public class WFS {

    private OpenApiResource openApi;

    private List collections = new ArrayList();

    @PostConstruct
    private void init() {
        openApi = new OpenApiResource();
        openApi.setConfigLocation("mosaics-openapi-config.json");

        Map annotationDescr = new HashMap();
        annotationDescr.put("name", "Annotations");
        annotationDescr.put("title", "Annotations WFS3 layer");
        annotationDescr.put("extent", "");
        collections.add(annotationDescr);
    }

    @Operation(summary = "Get annotation collections",
            description = "Get list of annotation collections")
    @Produces({MediaType.APPLICATION_JSON})
    @GET
    public Object getCollections() {
        Map result = new HashMap();
        result.put("collections", collections);

        return result;
    }

    @Path("api")
    public Class<WfsOpenApiResource> getOpenAPI() {
        return WfsOpenApiResource.class;
    }

    @Path("annotations")
    @Produces({MediaType.APPLICATION_JSON})
    public Object getAnnotations() {
        return WfsOpenApiResource.class;
    }

}
