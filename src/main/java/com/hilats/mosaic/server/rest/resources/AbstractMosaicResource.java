package com.hilats.mosaic.server.rest.resources;

import com.hilats.mosaic.server.MosaicServer;
import com.hilats.server.rest.resources.AbstractResource;

/**
 * @author pduchesne
 *         Created by pduchesne on 27/11/16.
 */
public class AbstractMosaicResource
    extends AbstractResource
{
    @Override
    public MosaicServer getApplication() {
        return (MosaicServer)super.getApplication();
    }
}
