package com.hilats.mosaic.server.rest.containers.wfs3;

import io.swagger.v3.jaxrs2.integration.JaxrsApplicationAndAnnotationScanner;

import java.util.Set;

/**
 * @author pduchesne
 *         Created by pduchesne on 10/02/18.
 */
public class MosaicsOpenapiScanner extends JaxrsApplicationAndAnnotationScanner {
    @Override
    public Set<Class<?>> classes() {
        Set<Class<?>> classes = super.classes();
        classes.removeIf(
                c -> !c.getTypeName().startsWith("com.hilats.mosaic.server.rest.containers.wfs3")
        );

        return classes;
    }
}
