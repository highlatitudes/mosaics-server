package com.hilats.mosaic.server;

import com.hilats.server.TripleStore;
import com.hilats.server.spring.jwt.HilatsUser;
import org.openrdf.model.Model;

/**
 * Created by pduchesne on 1/08/14.
 */
public interface MosaicTripleStore
    extends TripleStore {

    public static final String PREFIXES =
            "PREFIX moz:    <http://highlatitud.es/mosaics/>\n" +
            "PREFIX rdf:    <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "PREFIX dct:    <http://purl.org/dc/terms/>\n" +
            "PREFIX dc:    <http://purl.org/dc/elements/1.1/>\n" +
            "PREFIX oa:    <http://www.w3.org/ns/oa#>\n";

    public static final String DISTINCT_ANNOT_IDS =
            "PREFIX moz:    <http://highlatitud.es/mosaics/>\n" +
                    "PREFIX oa:    <http://www.w3.org/ns/oa#>\n" +
                    "SELECT ?annot ?iid \n" +
                    "WHERE { ?annot a oa:Annotation }";

    public static final String DISTINCT_MOZ_IDS =
            "PREFIX moz:    <http://highlatitud.es/mosaics/>\n" +
                    "SELECT ?mosaic ?iid \n" +
                    "WHERE { ?mosaic a moz:Mosaic. OPTIONAL { ?mosaic moz:iid ?iid } }";


    public static final String MOSAICS_SUMMARY =
            PREFIXES+
                    "CONSTRUCT { " +
                    " ?mosaic dc:title ?title . " +
                    " ?mosaic a moz:Mosaic. " +
                    " ?mosaic dc:creator ?creator . " +
                    " ?mosaic dct:modified ?modifiedDate . " +
                    " ?ann moz:inMosaic ?mosaic. " +
                    " ?ann a oa:Annotation. " +
                    " } \n" +
                    "WHERE { ?mosaic a moz:Mosaic." +
                    " OPTIONAL { ?mosaic dc:title ?title }" +
                    " OPTIONAL { ?mosaic dc:creator ?creator }" +
                    " OPTIONAL { ?mosaic dct:modified ?modifiedDate }" +
                    " OPTIONAL { ?ann a oa:Annotation.  ?ann moz:inMosaic ?mosaic. }" +
                    " FILTER (!bound(?query) || contains(lcase(?title), lcase(?query)))"+
                    "} "
            //+ "ORDER BY ?modifiedDate "
            ;

    public static final String MOSAICS_FULL =
            PREFIXES +
                    "CONSTRUCT { ?mosaic ?mosProp ?mosPropValue . " +
                    " ?res ?resProp1 ?resProp1Value ." +
                    " ?resProp1Value ?resProp2 ?resProp2Value ." +
                    " ?res moz:inMosaic ?mosaic ." +
                    " ?annFromRes dct:references ?res ." +
                    " ?annFromRes ?s3 ?o3 ." +
                    " ?annFromMosaic ?s3 ?o3 ." +
                    " ?annFromMosaic moz:inMosaic ?mosaic ." +
                    " ?o3 ?s4 ?o4 ." +
                    " ?o4 ?s5 ?o5 } \n" +
                    "WHERE { ?mosaic a moz:Mosaic." +
                    " ?mosaic ?mosProp ?mosPropValue." +
                    //" OPTIONAL { ?mosaic dct:modified ?modifiedDate }" +
                    " OPTIONAL { ?res a moz:Resource. ?res moz:inMosaic ?mosaic. ?res ?resProp1 ?resProp1Value. FILTER (?resProp1 != moz:inMosaic)  " +
                    "            OPTIONAL { ?resProp1Value ?resProp2 ?resProp2Value. FILTER (?resProp2 != moz:inMosaic && ?resProp2 != dct:references)} } " +
                    " OPTIONAL {  { ?annFromRes a oa:Annotation.  ?annFromRes dct:references ?res. ?annFromRes ?s3 ?o3. FILTER ( ?s3 != dct:references) } " +
                    "       UNION { ?annFromMosaic a oa:Annotation. ?annFromMosaic moz:inMosaic ?mosaic. ?annFromMosaic ?s3 ?o3. FILTER ( ?s3 != moz:inMosaic) } " +
                    "             OPTIONAL { ?o3 ?s4 ?o4. FILTER (?s4 != moz:inMosaic && ?s4 != dct:references) OPTIONAL { ?o4 ?s5 ?o5.  FILTER (?s5 != moz:inMosaic && ?s5 != dct:references)} } }" +
                    "} "
            //+ "ORDER BY ?modifiedDate "
            ;



    public static final String MOSAIC_BY_USER =
            PREFIXES +
                    "CONSTRUCT { ?mosaic ?mosProp ?mosPropValue . " +
                    " ?res ?resProp1 ?resProp1Value ." +
                    " ?resProp1Value ?resProp2 ?resProp2Value ." +
                    " ?res moz:inMosaic ?mosaic ." +
                    " ?annFromRes dct:references ?res ." +
                    " ?annFromRes ?s3 ?o3 ." +
                    " ?annFromMosaic ?s3 ?o3 ." +
                    " ?annFromMosaic moz:inMosaic ?mosaic ." +
                    " ?o3 ?s4 ?o4 ." +
                    " ?o4 ?s5 ?o5 } \n" +
                    "WHERE { ?mosaic a moz:Mosaic." +
                    " ?mosaic dc:creator ?user." +
                    " ?mosaic ?mosProp ?mosPropValue." +
                    " OPTIONAL { ?res a moz:Resource. ?res moz:inMosaic ?mosaic. ?res ?resProp1 ?resProp1Value. FILTER (?resProp1 != moz:inMosaic)  " +
                    "            OPTIONAL { ?resProp1Value ?resProp2 ?resProp2Value. FILTER (?resProp2 != moz:inMosaic && ?resProp2 != dct:references)} } " +
                    " OPTIONAL {  { ?annFromRes a oa:Annotation.  ?annFromRes dct:references ?res. ?annFromRes ?s3 ?o3. FILTER ( ?s3 != dct:references) } " +
                    "       UNION { ?annFromMosaic a oa:Annotation. ?annFromMosaic moz:inMosaic ?mosaic. ?annFromMosaic ?s3 ?o3. FILTER ( ?s3 != moz:inMosaic) } " +
                    "             OPTIONAL { ?o3 ?s4 ?o4. FILTER (?s4 != moz:inMosaic && ?s4 != dct:references) OPTIONAL { ?o4 ?s5 ?o5.  FILTER (?s5 != moz:inMosaic && ?s5 != dct:references)} } }" +
                    "} ";


    // this query generates a graph that adds inMosaic relations for found annotations
    public static final String MOSAIC_BY_URI_INFER_INMOSAIC =
           PREFIXES +
                    "CONSTRUCT { ?mosaic ?mosProp ?mosPropValue . " +
                    " ?res ?resProp1 ?resProp1Value ." +
                    " ?resProp1Value ?resProp2 ?resProp2Value ." +
                    " ?res moz:inMosaic ?mosaic ." +
                    " ?annFromRes dct:references ?res ." +
                    " ?annFromRes ?s3 ?o3 ." +
                    " ?annFromRes moz:inMosaic ?mosaic ." +
                    " ?annFromMosaic ?s3 ?o3 ." +
                    " ?annFromMosaic moz:inMosaic ?mosaic ." +
                    " ?o3 ?s4 ?o4 ." +
                    " ?o4 ?s5 ?o5 } \n" +
                    "WHERE { ?mosaic a moz:Mosaic." +
                    " ?mosaic ?mosProp ?mosPropValue." +
                    " OPTIONAL { ?res a moz:Resource. ?res moz:inMosaic ?mosaic. ?res ?resProp1 ?resProp1Value. FILTER (?resProp1 != moz:inMosaic)  " +
                    "            OPTIONAL { ?resProp1Value ?resProp2 ?resProp2Value. FILTER (?resProp2 != moz:inMosaic && ?resProp2 != dct:references)} } " +
                    " OPTIONAL {  { ?annFromRes a oa:Annotation.  ?annFromRes dct:references ?res. ?annFromRes ?s3 ?o3. FILTER ( ?s3 != dct:references) } " +
                    "       UNION { ?annFromMosaic a oa:Annotation. ?annFromMosaic moz:inMosaic ?mosaic. ?annFromMosaic ?s3 ?o3. FILTER ( ?s3 != moz:inMosaic) } " +
                    "             OPTIONAL { ?o3 ?s4 ?o4. FILTER (?s4 != moz:inMosaic && ?s4 != dct:references) OPTIONAL { ?o4 ?s5 ?o5.  FILTER (?s5 != moz:inMosaic && ?s5 != dct:references)} } }" +
                    "} ";

    public static final String RESOURCES =
            PREFIXES +
                    "CONSTRUCT { ?res ?resProp1 ?resProp1Value ." +
                    " ?res dc:title ?title ." +
                    " ?res moz:inMosaic ?moz ." +
                    " ?res a moz:Resource ." +
                    " ?resProp1Value ?resProp2 ?resProp2Value " +
                    "} \n" +
                    "WHERE {" +
                    "  { ?res a moz:Resource. ?res dc:title ?title. ?res moz:inMosaic ?moz.} UNION { ?ann a oa:Annotation. ?ann oa:hasTarget ?res. ?ann dc:title ?title. ?ann moz:inMosaic ?moz. FILTER (?expanded = true)}" +
                    " ?res oa:hasSource ?searchUrl." +  // TODO replace with EXISTS filter as below
                    " OPTIONAL { ?res ?resProp1 ?resProp1Value. FILTER (?resProp1 != moz:inMosaic)  }" +
                    " OPTIONAL { ?resProp1Value ?resProp2 ?resProp2Value. FILTER (?resProp2 != moz:inMosaic && ?resProp2 != dct:references)} " +
                    " FILTER (!bound(?query) || contains(lcase(?title), lcase(?query)))"+
                    "}";

    public static final String ANNOTATIONS_IDS_SUBQUERY =
            "SELECT ?ann {\n" +
            "    ?ann a oa:Annotation.\n" +
            "    OPTIONAL { ?ann dct:modified ?modifiedDate. }\n" +
            "    OPTIONAL { ?ann dc:title ?title. }" +
            "    OPTIONAL { ?ann moz:inMosaic ?moz.}" +
            "    FILTER (!bound(?query) || contains(lcase(?title), lcase(?query)))"+
            "    FILTER (!bound(?bodyUrl) || EXISTS {?ann oa:hasBody ?body. ?body oa:hasSource ?bodyUrl.} )"+
            "    FILTER (!bound(?targetUrl) || EXISTS {?ann oa:hasTarget ?target. ?target oa:hasSource ?targetUrl.} )"+
            "    FILTER (!bound(?linkType) || EXISTS {?ann oa:hasBody ?body. ?body moz:linkType ?linkType.} )"+
            "    FILTER (!bound(?user) || EXISTS {?ann dc:creator ?user.} )"+
            "}\n" +
            "ORDER BY DESC(?modifiedDate)";


    static String prepareAnnotationQuery(int count) {
        String annotSubquery = ANNOTATIONS_IDS_SUBQUERY;
        if (count >= 0) {
            annotSubquery += " LIMIT " + count;
        }

        return  PREFIXES +
                "CONSTRUCT {" +
                " ?ann ?s3 ?o3 ." +
                " ?ann moz:inMosaic ?moz ." +
                " ?moz ?mozPred ?mozProp ." +
                " ?ann dct:references ?res ." +
                " ?res dc:title ?resTitle ." +
                " ?o3 ?s4 ?o4 ." +
                " ?o4 ?s5 ?o5 } \n" +
                "WHERE {" +
                "     { "+ annotSubquery +" } \n" +
                "     {?ann ?s3 ?o3. FILTER (?s3 != moz:inMosaic && ?s3 != dct:references).}" +
                "     OPTIONAL { ?ann moz:inMosaic ?moz. " +
                "                OPTIONAL { ?moz ?mozPred ?mozProp FILTER (?mozPred IN (dc:creator, dc:title, dct:modified))}" +
                "                OPTIONAL { ?res moz:inMosaic ?moz. ?ann (oa:hasTarget|oa:hasBody)/oa:hasSource/^oa:hasSource ?res. ?res dc:title ?resTitle.}" +
                "     }" +
                "     OPTIONAL { ?o3 ?s4 ?o4. FILTER (?s4 != moz:inMosaic && ?s4 != dct:references) " +
                "                OPTIONAL { ?o4 ?s5 ?o5.  FILTER (?s5 != moz:inMosaic && ?s5 != dct:references)} } " +
                "}";

    }

    Object putMosaics(Model statements, HilatsUser writer);

    Object putMosaics(Model statements, String mosaicId, HilatsUser writer);

    Object getMosaic(String id);

    Object getMosaics();

    Object getMosaics(String text, String userId);

    Object getUserMosaics(String userId);

    boolean deleteMosaic(String id);

    Object putAnnotations(Model statements, HilatsUser writer);

    Object putAnnotation(Model statements, String annotId, HilatsUser writer);

    Object getAnnotation(String id);

    boolean deleteAnnotation(String id);

    Object getResources(String queryStr, String searchUrl, boolean expanded);

    Object getAnnotations(String queryStr, String linkType, String bodyUrl, String targetUrl, String userId, int limit);
}
