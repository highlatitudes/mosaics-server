package com.hilats.server.test;

import com.hilats.server.Main;
import com.hilats.server.RestRDFServer;
import com.jayway.jsonassert.JsonAssert;
import com.jayway.jsonpath.JsonPath;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Ignore;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.util.List;

public class AnnotationResourceTest
    extends AbstractResourceTest
{

    @Override
    public RestRDFServer setupServer() throws IOException {
        RestRDFServer server = new RestRDFServer(URI.create(Main.BASE_URI));
        server.startServer(URI.create(Main.BASE_URI), new String[]{"testApplicationContext.xml", "jersey-spring-applicationContext.xml"});
        return server;
    }


    @Test
    public void addAnnotationFromJson() {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/annotations/sample.json"), "application/json") ;

        // Put framed JSON
        Response putResponse = postWithSuccess("annotations", content);

        String getResponse = target.path("annotations.json").request().get(String.class);
        System.out.print(getResponse);

        JsonAssert.with(getResponse)
                .assertNotNull("[0].body.source");

    }




    @Test
    @Ignore // until test authentication is fixed
    //TODO probably not working - mere copy from mosaic test
    public void addNewAnnotationFromJson() {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/annotations/sample-new.json"), "application/json") ;

        // Put framed JSON
        Response putResponse = postWithSuccess("annotations", content);

        System.out.print(putResponse.readEntity(String.class));

        String getResponse = target.path("annotations.json").request().get(String.class);
        System.out.print(getResponse);

        // Check that the current user has only one annotation
        getResponse = target.path("annotations/mine.json").request().get(String.class);
        JsonAssert.with(getResponse)
                  .assertEquals("$.length()", 1);


    }


    @Test
    public void deleteAnnotation() throws IOException {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/annotations/sample.json"), "application/json") ;

        Response r = postWithSuccess("annotations", content);
        deleteWithSuccess("annotations/moz:annotation1");

        getWithStatus("annotations/moz:annotation1.json", 404);
    }

    @Test
    public void updateAnnotation() throws IOException {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/annotations/sample.json"), "application/json") ;

        postWithSuccess("annotations", content);
        String getResponse = target.path("annotations/moz:annotation1.json").request().get(String.class);

        System.out.print(getResponse);
        JsonAssert.with(getResponse)
                .assertEquals("id", "moz:annotation1");

        // update object


        content = Entity.entity(this.getClass().getResourceAsStream("/annotations/sample2.json"), "application/json") ;
        postWithSuccess("annotations", content);


        String stringResponse = target.path("annotations/moz:annotation1.json").request().get(String.class);
        System.out.print(stringResponse);
        JsonAssert.with(stringResponse)
                .assertEquals("id", "moz:annotation1")
                .assertEquals("target.selector.value", "t=55,58");


    }

    @Test
    public void getAnnotationById() throws IOException {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/annotations/sample-new.json"), "application/json") ;

        Response putResponse = postWithSuccess("annotations", content);
        String newId = JsonPath.read((InputStream) putResponse.getEntity(), "[0]");

        Matcher noBlankNodesMatcher = new BaseMatcher() {
            public boolean matches(Object o) { return !((String) o).startsWith("_:");}
            public void describeTo(Description description) {}
        };

        // test unexisting annotation
        getWithStatus("annotations/moz:1234.json", Response.Status.NOT_FOUND.getStatusCode());

        // retrieve newly created annotation
        Response getResponse = getWithSuccess("annotations/" + URLEncoder.encode(newId) + ".json");


        JsonAssert.with((InputStream) getResponse.getEntity())
                .assertThat("id", noBlankNodesMatcher);

    }

}
