package com.hilats.server.test;

import com.github.jsonldjava.utils.JsonUtils;
import com.hilats.server.Main;
import com.hilats.server.RestRDFServer;
import com.jayway.jsonassert.JsonAssert;
import com.jayway.jsonpath.JsonPath;
import org.codehaus.jackson.map.util.ISO8601Utils;
import org.glassfish.grizzly.http.server.HttpServer;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.collection.IsArrayWithSize;
import org.junit.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MosaicResourceTest
    extends AbstractResourceTest
{

    @Override
    public RestRDFServer setupServer() throws IOException {
        RestRDFServer server = new RestRDFServer(URI.create(Main.BASE_URI));
        server.startServer(URI.create(Main.BASE_URI), new String[]{"testApplicationContext.xml", "jersey-spring-applicationContext.xml"});
        return server;
    }

    @Test
    public void addMosaicFromJsonld() {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/sample.jsonld"), "application/ld+json") ;

        Response postResponse = postWithSuccess("mosaics", content);

        String getResponse = target.path("mosaics.jsonld").request().get(String.class);
        System.out.print(getResponse);

        getResponse = target.path("mosaics.json").request().get(String.class);
        System.out.print(getResponse);
        JsonAssert.with(getResponse)
                .assertEquals("[0].annotations.length()", 1)
                .assertNotNull("[0].annotations[0].body.source");
    }

    @Test
    public void addMosaicFromFramedJsonld() {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/sample-framed.jsonld"), "application/ld+json") ;

        Response putResponse = postWithSuccess("mosaics", content);

        String getResponse = target.path("mosaics.jsonld").request().get(String.class);
        System.out.print(getResponse);

        getResponse = target.path("mosaics.json").request().get(String.class);
        System.out.print(getResponse);
        JsonAssert.with(getResponse)
                .assertEquals ("[0].annotations.length()", 1)
                .assertNotNull("[0].annotations[0].body.source");

    }


    @Test
    public void addMosaicFromJson() {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/sample.json"), "application/json") ;

        // Put framed JSON
        Response putResponse = postWithSuccess("mosaics", content);

        String getResponse = target.path("mosaics.json").request().get(String.class);
        System.out.print(getResponse);

        JsonAssert.with(getResponse)
                .assertEquals ("[0].annotations.length()", 1)
                .assertNotNull("[0].annotations[0].body.source");

    }

    @Test
    public void addMosaicWithJsonLiteral() {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/sample-literal.json"), "application/json") ;

        // Put framed JSON
        Response putResponse = postWithSuccess("mosaics", content);

        String getResponse = target.path("mosaics.json").request().get(String.class);
        System.out.print(getResponse);

        JsonAssert.with(getResponse)
                .assertEquals ("[0].resources.length()", 1)
                .assertNotNull("[0].resources[0]['vizConfig']");

    }

    @Test
    public void getResources() {
        // Fill DB
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/sample.json"), "application/json") ;
        postWithSuccess("mosaics", content);

        String getResponse = target.path("resources.json").request().get(String.class);
        System.out.print(getResponse);

        JsonAssert.with(getResponse)
                .assertEquals ("length()", 3)
                .assertNotNull("[0].selector");

    }

    @Test
    public void addMosaicFromJson2() {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/sample2.json"), "application/json") ;

        // Put framed JSON
        Response putResponse = postWithSuccess("mosaics", content);

        String getResponse = target.path("mosaics.json").request().get(String.class);
        System.out.print(getResponse);

        JsonAssert.with(getResponse)
                .assertEquals ("[0].annotations.length()", 2)
                .assertNotNull("[0].annotations[0].body.source");

    }


    @Test
    @Ignore // until test authentication is fixed
    public void addNewMosaicFromJson() {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/sample-new.json"), "application/json") ;

        // Put framed JSON
        Response putResponse = postWithSuccess("mosaics", content);

        System.out.print(putResponse.readEntity(String.class));

        String getResponse = target.path("mosaics.json").request().get(String.class);
        System.out.print(getResponse);

        // Check that the current user has only one mosaic
        getResponse = target.path("mosaics/mine.json").request().get(String.class);
        JsonAssert.with(getResponse)
                  .assertEquals("$.length()", 1);


    }


    @Test
    public void addMosaicFromTurtle() {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/sample.turtle"), "text/turtle") ;

        Response putResponse = postWithSuccess("mosaics", content);

        String getResponse = target.path("mosaics.ttl").request().get(String.class);
        System.out.print(getResponse);

        getResponse = target.path("mosaics.json").request().get(String.class);
        System.out.print(getResponse);

        JsonAssert.with(getResponse)
                .assertEquals ("[0].annotations.length()", 1)
                .assertNotNull("[0].annotations[0].body.source");
    }

    @Test
    public void deleteMosaic() throws IOException {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/sample.turtle"), "text/turtle") ;

        postWithSuccess("mosaics", content);
        deleteWithSuccess("mosaics/moz:mosaic1");

        getWithStatus("mosaics/moz:mosaic1.json", 404);
    }

    @Test
    public void updateMosaic() throws IOException, ParseException {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/sample.turtle"), "text/turtle") ;

        postWithSuccess("mosaics", content);
        String getResponse = target.path("mosaics.json").request().get(String.class);



        System.out.print(getResponse);
        JsonAssert.with(getResponse)
                .assertEquals("[0].id", "moz:mosaic1");
        String modifiedDate = JsonPath.read(getResponse, "[0].modified");
        // update object


        content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/sample2.turtle"), "text/turtle") ;
        postWithSuccess("mosaics", content);


        String stringResponse = target.path("mosaics/moz:mosaic1.json").request().get(String.class);
        System.out.print(stringResponse);
        JsonAssert.with(stringResponse)
                .assertEquals("id", "moz:mosaic1")
                .assertThat("resources", new BaseMatcher<Object>() {
                    public boolean matches(Object o) {
                        return o != null && ((List)o).size() == 1;
                    }
                    public void describeTo(Description description) {}
                })
                .assertEquals("resources[0].id", "moz:resource2");

        String newModifiedDate = JsonPath.read(stringResponse, "modified");

        Assert.assertTrue(ISO8601Utils.parse(modifiedDate).getTime() < ISO8601Utils.parse(newModifiedDate).getTime());


    }


    @Test
    public void addBatchNew() {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/batch-new.turtle"), "text/turtle") ;

        Response putResponse = postWithSuccess("mosaics", content);

        String getResponse = target.path("mosaics.jsonld").request().get(String.class);
        System.out.print(getResponse);
    }



    @Test
    public void getMosaicById() throws IOException {
        Entity content = Entity.entity(this.getClass().getResourceAsStream("/mosaics/sample-new.json"), "application/json") ;

        Response putResponse = postWithSuccess("mosaics", content);
        String newId = JsonPath.read((InputStream) putResponse.getEntity(), "[0]");

        Matcher noBlankNodesMatcher = new BaseMatcher() {
            public boolean matches(Object o) { return !((String) o).startsWith("_:");}
            public void describeTo(Description description) {}
        };

        // test unexisting mosaic
        getWithStatus("mosaics/moz:1234.json", Response.Status.NOT_FOUND.getStatusCode());

        // retrieve newly created mosaic
        Response getResponse = getWithSuccess("mosaics/" + URLEncoder.encode(newId) + ".json");


        JsonAssert.with((InputStream) getResponse.getEntity())
                .assertThat("id", noBlankNodesMatcher)
                .assertThat("resources[0].id", noBlankNodesMatcher);

    }

}
