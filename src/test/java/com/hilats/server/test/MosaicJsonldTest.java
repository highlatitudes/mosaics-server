package com.hilats.server.test;

//import com.github.jsonldjava.jena.JenaJSONLD;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.github.jsonldjava.core.DocumentLoader;
import com.github.jsonldjava.core.JsonLdError;
import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;
import com.hilats.mosaic.server.MosaicDocumentLoader;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


public class MosaicJsonldTest
{

    @Before
    public void init() {
        //JenaJSONLD.init(); //TODO code moved into jena project
    }

    @Test
    public void testFraming() throws IOException, JsonLdError {

        Object frame = //JsonUtils.fromInputStream(this.getClass().getResourceAsStream("/jsonld/mosaic-frame.jsonld"));
                       //this.getClass().getResource("/jsonld/mosaic-frame.jsonld").toString();
                       "http://highlatitud.es/mosaics/mosaic-frame.jsonld";

        Object input = JsonUtils.fromInputStream(this.getClass().getResourceAsStream("/mosaics/sample.jsonld"));
                       //this.getClass().getResource("/mosaics/sample.jsonld").toString();
        JsonLdOptions opts = new JsonLdOptions(null);
        DocumentLoader loader = new MosaicDocumentLoader();
        opts.setDocumentLoader(loader);

        Object framed = JsonLdProcessor.frame(input, frame, opts);

        System.out.println(JsonUtils.toPrettyString(framed));



    }


    @Test
    public void testFlattening() throws IOException, JsonLdError {

        Object frame = //JsonUtils.fromInputStream(this.getClass().getResourceAsStream("/jsonld/mosaic-frame.jsonld"));
                //this.getClass().getResource("/jsonld/mosaic-frame.jsonld").toString();
                "http://highlatitud.es/mosaics/mosaic-frame.jsonld";

        Object input = JsonUtils.fromInputStream(this.getClass().getResourceAsStream("/mosaics/sample.json"));
        //this.getClass().getResource("/mosaics/sample.jsonld").toString();

        DocumentLoader loader = new MosaicDocumentLoader();
        JsonLdOptions opts = new JsonLdOptions(null);
        opts.setDocumentLoader(loader);


        // parse with Options to force setting the base URL and ensure proper resolution of remote context
        frame = JsonLdProcessor.parseInputIfPossible(frame, opts);

        Map jsonld = new HashMap();
        jsonld.put("@graph", input);
        jsonld.put("@context", ((Map)frame).get("@context"));

        Object flattened = JsonLdProcessor.flatten(jsonld, opts);
        Object compacted = JsonLdProcessor.compact(flattened, ((Map)frame).get("@context"), opts);
        System.out.println(JsonUtils.toPrettyString(compacted));



    }

}
